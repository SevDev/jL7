# jL7

>>>
jL7 is an HL7 library for Java.

It provides the following:
- An HL7 v2 parser (pipes)
- An implementation of MLLP
- Some useful helpers to work with HL7 messages
- A Domain Specific Language based on Groovy to process and convert HL7 messages

Please feel free to use this library or contribute to it !

*See http://benohead.com/jl7-hl7-library-for-java/'>the jL7 page or directly http://benohead.com/category/jl7/'>my blog entries for more information.*
>>>

---

Imported from Google Code Archive's repo [jL7](https://code.google.com/archive/p/jl7/).
